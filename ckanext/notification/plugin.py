import ckan.plugins as plugins
import ckan.plugins.toolkit as tk
import logging
import requests

class NotificationPlugin(plugins.SingletonPlugin):
    plugins.implements(plugins.IDomainObjectModification)

    def notify(self, entity, operation):
        logging.warning('notify: entity %s ; operation %s', entity, operation) #example: notify: entity <Package id=069f24a0-8246-4aaa-aafd-54ff98a22629 name=test2 title=test2 version= url= author= author_email= maintainer= maintainer_email= notes=test2 license_id=cc-by type=dataset owner_org=a4ff2ba0-cf2a-475b-a209-0ff0edaf5645 creator_user_id=dab6db9b-fb75-4dba-a27e-144ee7c8d0a2 metadata_created=2023-04-26 16:25:22.070911 metadata_modified=2023-04-26 16:25:22.070917 private=True state=draft> ; operation new
        #example2: notify: entity <Package id=18296d16-0c8c-4196-83ef-c5c99b658616 name=test3 title=test3 version= url= author= author_email= maintainer= maintainer_email= notes=trest3sfsdf s sd sdf license_id=cc-by type=dataset owner_org=a4ff2ba0-cf2a-475b-a209-0ff0edaf5645 creator_user_id=dab6db9b-fb75-4dba-a27e-144ee7c8d0a2 metadata_created=2023-04-27 10:09:22.345879 metadata_modified=2023-04-27 11:07:29.760350 private=True state=active> ; operation changed

        # Only trigger whe new files were added
        if operation == "upload": # TODO
            data = "files="
            for index, item in entity["extra"]:
                if index == "drawio":
                    data += item["value"] # TODO

            data += ";namespace="
            for index, item in entity["extra"]:
                if index == "namespace":
                    data += item["value"] # TODO

            data += ";name="+entity["name"]

            url = "" # TODO
            headers = {
                "Authorization": "" # TODO
            }

            response = requests.post(url, headers=headers, data=data)
            logging.warning('response from webhook: %s', response)

        ## Example for sending emails
        #email = "test@test.de"
        #tk.mail_recipient('System Admin', email, "subject", "Some Body")